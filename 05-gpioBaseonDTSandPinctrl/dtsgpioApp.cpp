#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#define LEDOFF  1
#define LEDON   0

int main(int argc, char **argv) {
    int fd, retvalue;
    char *filename;
    unsigned char databuf[1];

    if(argc != 3 && argc != 4) {
        printf("Usage:\n\r");
        printf("%s /dev/something 1 : this means read the value of gpio\n\r", argv[0]);
        printf("%s /dev/something 2 ON : this means change the value of gpio to 0\n\r", argv[0]);
        printf("%s /dev/something 2 OFF : this means change the value of gpio to 1\n\r", argv[0]);
        exit(0);
    }
    filename = argv[1];
    fd = open(filename, O_RDWR);
    if(fd < 0) {
        printf("led file %s open failed\n\r", argv[1]);
        return -1;        
    }

    if(argc == 3) {
        retvalue = read(fd, databuf, sizeof(databuf));
        if(retvalue < 0) {
            printf("gpio read failed\n\r");
            close(fd);
            return -1;
        }
        printf("gpio value: %c\n\r", databuf[0]);
    }
    else if(argc == 4) {
        if(!strncmp(argv[3], "OFF", 3)) {
            databuf[0] = '1';
            retvalue = write(fd, databuf, 1);
            if(retvalue < 0) {
                printf("gpio write failed\n\r");
                close(fd);
                return -1;
            }
            printf("write 1 to gpio\n\r");            
        }
        else if(!strncmp(argv[3], "ON", 2)) {
            databuf[0] = '0';
            retvalue = write(fd, databuf, 1);
            if(retvalue < 0) {
                printf("gpio write failed\n\r");
                close(fd);
                return -1;
            }
            printf("write 0 to gpio\n\r");               
        }
        else {
            printf("Wrong Usage\n\r");
        }
    }

    close(fd);
    return 0;
}