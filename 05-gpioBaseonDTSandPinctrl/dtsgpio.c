#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/mach/map.h>
#include <asm/uaccess.h>
#include <asm/io.h>

#define GPIOLED_NAME "gpioledpinctrl"
#define GPIOLED_CNT  (1)
#define LEDOFF      1
#define LEDON       0

struct gpioled_dev {
    dev_t devid;
    struct cdev cdev;
    struct class *class;
    struct device *device;
    struct device_node *nd;
    int gpioled_gpio;   /* gpio所使用的gpio编号 */
};

struct gpioled_dev gpioled;

static int led_open(struct inode *inode, struct file *filp) {
    filp->private_data = &gpioled;
    return 0;
}

static ssize_t led_read(struct file *filp, char __user *buf, size_t cnt, loff_t *offt) {
    struct gpioled_dev *dev = filp->private_data;
    int retvalue;
    char ledStatus[1];

    retvalue = gpio_get_value(dev->gpioled_gpio);
    if(retvalue < 0) {
        printk("gpio_get_value failed\n\r");
        return -EFAULT;
    }
    printk("[kernel]: gpio %d retvalue:%d\n\r", dev->gpioled_gpio, retvalue);
    ledStatus[0] = (char)(retvalue + '0');
    
    retvalue = copy_to_user(buf, ledStatus, cnt);
    if(retvalue == 0) {
        printk("kernel senddata ok\n\r");
    }
    else {
        printk("kernel senddata failed\n\r");
        return -EFAULT;
    }
    
    return 0;
}

static ssize_t led_write(struct file *filp, const char __user *buf, size_t cnt, loff_t *offt) {
    int retvalue;
    unsigned char databuf[1];
    unsigned char ledstat;
    struct gpioled_dev *dev = filp->private_data;

    retvalue = copy_from_user(databuf, buf, cnt);
    if(retvalue < 0) {
        printk("kernel write failed\n\r");
        return -EFAULT;
    }
    printk("[kernel] ready to change gpio value\n\r");

    ledstat = databuf[0];
    //printk("ledstat = %c\n\r", ledstat);
    
    if(ledstat == '0') {
        gpio_set_value(dev->gpioled_gpio, 0);
        printk("[kernel] set gpio %d to 0\n\r", dev->gpioled_gpio);
    }
    else if(ledstat == '1') {
        gpio_set_value(dev->gpioled_gpio, 1);
        printk("[kernel] set gpio %d to 1\n\r", dev->gpioled_gpio);
    }

    return 0;
}

static int led_release(struct inode *inode, struct file *filp) {
    return 0;
}

static struct file_operations gpioled_fops = {
    .owner = THIS_MODULE,
    .open = led_open,
    .read = led_read,
    .write = led_write,
    .release = led_release
};

static int __init led_init(void) {
    int ret = 0;

    gpioled.nd = of_find_node_by_path("/mygpioled");
    if(gpioled.nd == NULL) {
        printk("mygpioled node can not found\n\r");
        return -EINVAL;
    }
    
    gpioled.gpioled_gpio = of_get_named_gpio(gpioled.nd, "spgpio-led", 0);
    if(gpioled.gpioled_gpio < 0) {
        printk("can't get spgpio-led\n\r");
        return -EINVAL;
    }
    printk("spgpio-led num = %d\n\r", gpioled.gpioled_gpio);

    ret = gpio_direction_output(gpioled.gpioled_gpio, 0);
    if(ret < 0) {
        printk("cat't set gpio!\n\r");
    }

        /* 2、注册设备驱动 */
    /* 初始化cdev */
    cdev_init(&gpioled.cdev, &gpioled_fops);
    gpioled.cdev.owner = THIS_MODULE;
    printk("init cdev done.\n\r");

    /* 自动获取字符设备号*/
    alloc_chrdev_region(&gpioled.devid, 0, GPIOLED_CNT, GPIOLED_NAME);
    printk("alloc chrdev region done.\n\r");

    /* 注册设备 */
    ret = cdev_add(&gpioled.cdev, gpioled.devid, GPIOLED_CNT);
    if(ret < 0)
    {
        printk("cdev_add failed\n\r");
    }
    printk("cdev add done.\n\r");

    /* 创建设备的类，在/sys/class文件夹下 */
    gpioled.class = class_create(THIS_MODULE, GPIOLED_NAME);
    if(IS_ERR(gpioled.class)) {
        return PTR_ERR(gpioled.class);
    }
    printk("class create done.\n\r");

    /* 自动创建设备节点文件，在/dev文件夹下 */
    //curr_dev = MKDEV(MAJOR(dtsled.devid), MINOR(dtsled.devid));
    gpioled.device = device_create(gpioled.class, NULL, gpioled.devid, NULL, GPIOLED_NAME);
    if(IS_ERR(gpioled.device)) {
        return PTR_ERR(gpioled.device);
    }
    printk("mygpioled init\n\r");

    return 0;
}

static void __exit led_exit(void) {
    //dev_t curr_dev;

    /* 删除设备节点文件 */
    //curr_dev = MKDEV(MAJOR(dtsled.devid), MINOR(dtsled.devid));
    device_destroy(gpioled.class, gpioled.devid);
    
    /* 删除设备的类 */
    class_destroy(gpioled.class);

    /* 注销设备 */
    cdev_del(&gpioled.cdev);
    
    /* 释放占用的设备号 */
    unregister_chrdev_region(gpioled.devid, GPIOLED_CNT);
 
    printk("mygpioled_exit\n\r");
}

module_init(led_init);
module_exit(led_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sigmapoet");