/*
Name:   led灯驱动开发实验
Date:   20221026
Note:   地址和地址对应的值千万不要设置错了。
*/

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>              // 在vscode里面这个会报include错误，追溯是找不到mach/irqs.h，但是好像不影响编译
#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>

#define MYLED_NAME "myled"

#define LEDOFF       (0)
#define LEDON        (1)

/* 寄存器物理地址 */
#define CCM_CCGR1_BASE          (0X020C406C)
#define SW_MUX_GPIO1_IO03_BASE  (0X020E0068)
#define SW_PAD_GPIO1_IO03_BASE  (0X020E02F4)
#define GPIO1_DR_BASE           (0X0209C000)
#define GPIO1_GDIR_BASE         (0X0209C004)

/* 映射后的寄存器虚拟地址指针 */
static void __iomem *IMX6U_CCM_CCGR1;
static void __iomem *SW_MUX_GPIO1_IO03;
static void __iomem *SW_PAD_GPIO1_IO03;
static void __iomem *GPIO1_DR;
static void __iomem *GPIO1_GDIR;

/* 下面这样定义不太专业，对一个设备的所有属性信息，最好做成一个结构体 */
struct class *myled_class;
struct cdev myled_cdev;
dev_t dev_num;

static void led_switch(unsigned char status) {
    unsigned int val = 0;
    if(status == LEDON) {
        val = readl(GPIO1_DR);
        val &= ~(1 << 3);
        writel(val, GPIO1_DR);
    }
    else if(status == LEDOFF) {
        val = readl(GPIO1_DR);
        val |= (1 << 3);
        writel(val, GPIO1_DR);
    }
}

static int myled_open(struct inode *inode, struct file *filp) {
    return 0;
}

static int myled_read(struct file *filp, char __user *buf, size_t cnt, loff_t *offt) {
    return 0;
}

static ssize_t myled_write(struct file *filp, const char __user *buf, size_t cnt, loff_t *offt) {
    int retvalue;
    unsigned char databuf[1];
    unsigned char ledstat;

    retvalue = copy_from_user(databuf, buf, cnt);
    if(retvalue < 0) {
        printk("kernel write failed\n\r");
        return -EFAULT;
    }

    ledstat = databuf[0];
    if(ledstat == LEDON) {
        led_switch(LEDON);
    }
    else if(ledstat == LEDOFF) {
        led_switch(LEDOFF);
    }
    return 0;
}

static int myled_release(struct inode *inode, struct file *filp) {
    return 0;
}

static struct file_operations myled_fops = {
    .owner = THIS_MODULE,
    .open = myled_open,
    .read = myled_read,
    .write = myled_write,
    .release = myled_release,
};

static int __init myled_init(void) {
    int retvalue = 0;
    unsigned int val = 0;
    dev_t curr_dev;

    /* 1、初始化LED，看芯片手册确定下面的东西 */
    /* (1)寄存器地址映射 */
    printk("init ioremap...\n\r");
    
    IMX6U_CCM_CCGR1 = ioremap(CCM_CCGR1_BASE, 4);
    SW_MUX_GPIO1_IO03 = ioremap(SW_MUX_GPIO1_IO03_BASE, 4);
    SW_PAD_GPIO1_IO03 = ioremap(SW_PAD_GPIO1_IO03_BASE, 4);
    GPIO1_DR = ioremap(GPIO1_DR_BASE, 4);
    GPIO1_GDIR = ioremap(GPIO1_GDIR_BASE, 4);
    
    printk("init ioremap done.\n\r");
    
    /* (2)使用GPIO1时钟 */
    printk("init gpio clock...\n\r");
    
    val = readl(IMX6U_CCM_CCGR1);
    val &= ~(3 << 26); /* 清除以前的设置 */
    val |= (3 << 26); /* 设置新值 */
    writel(val, IMX6U_CCM_CCGR1);

    printk("init gpio clock done.\n\r");

    /* (3)设置GPIO1_IO03的复用功能和属性 */
    printk("set GPIO1_IO03...\n\r");

    writel(5, SW_MUX_GPIO1_IO03);
    printk("set SW_MUX_GPIO1_IO03 done.\n\r");

    writel(0x10B0, SW_PAD_GPIO1_IO03);
    printk("set SW_PAD_GPIO1_IO03 done.\n\r");
    
    /* (4)设置GPIO1_IO03为输出功能 */
    val = readl(GPIO1_GDIR);
    val &= ~(1 << 3);
    val |= (1 << 3);
    writel(val, GPIO1_GDIR);
    printk("set GPIO1_GDIR done.\n\r");

    /* (5)默认关闭LED */
    val = readl(GPIO1_DR);
    val |= (1 << 3);
    writel(val, GPIO1_DR);

    printk("set GPIO1_IO03 done.\n\r");

    /* 2、注册设备驱动 */
    /* 初始化cdev */
    cdev_init(&myled_cdev, &myled_fops);
    myled_cdev.owner = THIS_MODULE;
    printk("init cdev done.\n\r");

    /* 自动获取字符设备号*/
    alloc_chrdev_region(&dev_num, 0, 1, MYLED_NAME);
    printk(" alloc chrdev region done.\n\r");

    /* 注册设备 */
    retvalue = cdev_add(&myled_cdev, dev_num, 1);
    if(retvalue < 0)
    {
        printk("cdev_add failed\n\r");
    }
    printk("cdev add done.\n\r");

    /* 创建设备的类，在/sys/class文件夹下 */
    myled_class = class_create(THIS_MODULE, MYLED_NAME);
    printk("class create done.\n\r");

    /* 自动创建设备节点文件，在/dev文件夹下 */
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num));
    device_create(myled_class, NULL, curr_dev, NULL, MYLED_NAME);

    printk("myled init\n\r");

    return 0;
}

static void __exit myled_exit(void) {
    dev_t curr_dev;

    /* 删除设备节点文件 */
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num));
    device_destroy(myled_class, curr_dev);
    
    /* 删除设备的类 */
    class_destroy(myled_class);

    /* 注销设备 */
    cdev_del(&myled_cdev);
    
    /* 释放占用的设备号 */
    unregister_chrdev_region(dev_num, 1);
 
    printk("myled_exit\n\r");
}

module_init(myled_init);
module_exit(myled_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sigmapoet");