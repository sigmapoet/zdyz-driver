#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/mach/map.h>
#include <asm/uaccess.h>
#include <asm/io.h>

#define DTSLED_NAME "dtsled"
#define DTSLED_CNT  (1)
#define LEDOFF      0
#define LEDON       1

/* 映射后的寄存器虚拟地址指针 */
static void __iomem *IMX6U_CCM_CCGR1;
static void __iomem *SW_MUX_GPIO1_IO03;
static void __iomem *SW_PAD_GPIO1_IO03;
static void __iomem *GPIO1_DR;
static void __iomem *GPIO1_GDIR;

/* dstled设备结构体 */  /* 这是个专业的写法，同一个设备的属性啥的写在一个结构里面 */
struct dtsled_dev {
    dev_t devid;
    struct cdev cdev;
    struct class *class;
    struct device *device;
    struct device_node *nd;
};

struct dtsled_dev dtsled;   /* led设备实例 */

static void led_switch(unsigned char status) {
    unsigned int val = 0;
    if(status == LEDON) {
        val = readl(GPIO1_DR);
        val &= ~(1 << 3);
        writel(val, GPIO1_DR);
    }
    else if(status == LEDOFF) {
        val = readl(GPIO1_DR);
        val |= (1 << 3);
        writel(val, GPIO1_DR);
    }
}

static int dtsled_open(struct inode *inode, struct file *filp) {
    filp->private_data = &dtsled;   /* 设置私有数据 */
    return 0;
}

static int dtsled_read(struct file *filp, char __user *buf, size_t cnt, loff_t *offt) {
    return 0;
}

static ssize_t dtsled_write(struct file *filp, const char __user *buf, size_t cnt, loff_t *offt) {
    int retvalue;
    unsigned char databuf[1];
    unsigned char ledstat;

    retvalue = copy_from_user(databuf, buf, cnt);
    if(retvalue < 0) {
        printk("kernel write failed\n\r");
        return -EFAULT;
    }

    ledstat = databuf[0];
    if(ledstat == LEDON) {
        led_switch(LEDON);
    }
    else if(ledstat == LEDOFF) {
        led_switch(LEDOFF);
    }
    return 0;
}

static int dtsled_release(struct inode *inode, struct file *filp) {
    return 0;
}

static struct file_operations dtsled_fops = {
    .owner = THIS_MODULE,
    .open = dtsled_open,
    .read = dtsled_read,
    .write = dtsled_write,
    .release = dtsled_release,
};

static int __init dtsled_init(void) {
    int retvalue = 0;
    unsigned int val = 0;
    unsigned int regdata[14];
    const char *str;
    struct property *proper;

    /* 获取设备树中的属性数据 */
    /* 1、获取设备节点：alphaled */
    dtsled.nd = of_find_node_by_path("/alphaled");
    if(dtsled.nd == NULL) {
        printk("alphaled node can not fount\n\r");
        return -EINVAL;
    }
    else {
        printk("alphaled node has been found\n\r");
    }
    /* 2、获取compatible属性内容 */
    proper = of_find_property(dtsled.nd, "compatible", NULL);
    if(proper == NULL) {
        printk("compatible property find failed\n\r");
    }
    else {
        printk("compatible = %s\n\r", (char*)proper->value);
    }
    /* 3、获取status属性内容 */
    retvalue = of_property_read_string(dtsled.nd, "status", &str);
    if(retvalue < 0) {
        printk("status read failed\n\r");
    }
    else {
        printk("status = %s\n\r", str);
    }
    /* 4、获取reg属性内容 */
    retvalue = of_property_read_u32_array(dtsled.nd, "reg", regdata, 10);
    if(retvalue < 0) {
        printk("reg property read failed\n\r");
    }
    else {
        unsigned int i = 0;
        printk("reg data:\n\r");
        for(i = 0; i < 10; ++ i)
            printk("%#X ", regdata[i]);
        printk("\n\r");
    }
    /* 1、初始化LED，看芯片手册确定下面的东西 */
    /* (1)寄存器地址映射 */
    printk("init ioremap...\n\r");
    
    IMX6U_CCM_CCGR1 = of_iomap(dtsled.nd, 0);
    SW_MUX_GPIO1_IO03 = of_iomap(dtsled.nd, 1);
    SW_PAD_GPIO1_IO03 = of_iomap(dtsled.nd, 2);
    GPIO1_DR = of_iomap(dtsled.nd, 3);
    GPIO1_GDIR = of_iomap(dtsled.nd, 4);
    
    printk("init ioremap done.\n\r");
    
    /* (2)使用GPIO1时钟 */
    printk("init gpio clock...\n\r");
    
    val = readl(IMX6U_CCM_CCGR1);
    val &= ~(3 << 26); /* 清除以前的设置 */
    val |= (3 << 26); /* 设置新值 */
    writel(val, IMX6U_CCM_CCGR1);

    printk("init gpio clock done.\n\r");

    /* (3)设置GPIO1_IO03的复用功能和属性 */
    printk("set GPIO1_IO03...\n\r");

    writel(5, SW_MUX_GPIO1_IO03);
    printk("set SW_MUX_GPIO1_IO03 done.\n\r");

    writel(0x10B0, SW_PAD_GPIO1_IO03);
    printk("set SW_PAD_GPIO1_IO03 done.\n\r");
    
    /* (4)设置GPIO1_IO03为输出功能 */
    val = readl(GPIO1_GDIR);
    val &= ~(1 << 3);
    val |= (1 << 3);
    writel(val, GPIO1_GDIR);
    printk("set GPIO1_GDIR done.\n\r");

    /* (5)默认关闭LED */
    val = readl(GPIO1_DR);
    val |= (1 << 3);
    writel(val, GPIO1_DR);

    printk("set GPIO1_IO03 done.\n\r");

    /* 2、注册设备驱动 */
    /* 初始化cdev */
    cdev_init(&dtsled.cdev, &dtsled_fops);
    dtsled.cdev.owner = THIS_MODULE;
    printk("init cdev done.\n\r");

    /* 自动获取字符设备号*/
    alloc_chrdev_region(&dtsled.devid, 0, DTSLED_CNT, DTSLED_NAME);
    printk(" alloc chrdev region done.\n\r");

    /* 注册设备 */
    retvalue = cdev_add(&dtsled.cdev, dtsled.devid, DTSLED_CNT);
    if(retvalue < 0)
    {
        printk("cdev_add failed\n\r");
    }
    printk("cdev add done.\n\r");

    /* 创建设备的类，在/sys/class文件夹下 */
    dtsled.class = class_create(THIS_MODULE, DTSLED_NAME);
    if(IS_ERR(dtsled.class)) {
        return PTR_ERR(dtsled.class);
    }
    printk("class create done.\n\r");

    /* 自动创建设备节点文件，在/dev文件夹下 */
    //curr_dev = MKDEV(MAJOR(dtsled.devid), MINOR(dtsled.devid));
    dtsled.device = device_create(dtsled.class, NULL, dtsled.devid, NULL, DTSLED_NAME);
    if(IS_ERR(dtsled.device)) {
        return PTR_ERR(dtsled.device);
    }
    printk("myled init\n\r");

    return 0;
}

static void __exit dtsled_exit(void) {
    //dev_t curr_dev;

    /* 删除设备节点文件 */
    //curr_dev = MKDEV(MAJOR(dtsled.devid), MINOR(dtsled.devid));
    device_destroy(dtsled.class, dtsled.devid);
    
    /* 删除设备的类 */
    class_destroy(dtsled.class);

    /* 注销设备 */
    cdev_del(&dtsled.cdev);
    
    /* 释放占用的设备号 */
    unregister_chrdev_region(dtsled.devid, DTSLED_CNT);
 
    printk("myled_exit\n\r");
}

module_init(dtsled_init);
module_exit(dtsled_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sigmapoet");