/*
Name:   字符设备驱动开发实验
Date:   20220825
*/

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>              // 在vscode里面这个会报include错误，追溯是找不到mach/irqs.h，但是好像不影响编译
#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>

#include "chr_ioctl.h"

#define CHRDEVBASE_MAJOR    200
#define CHRDEVBASE_NAME     "chrdevbase"

struct class *chrdevbase_class;
struct cdev chrdevbase_cdev;
dev_t dev_num;

static char readbuf[100];
static char writebuf[100];
static char kerneldata[] = {"kernel data!"};

/* 如果open方法没有定义，则打开总是成功 */
static int chrdevbase_open(struct inode *inode, struct file *filp) {
    return 0;
}

static int chrdevbase_read(struct file *filp, char __user *buf, size_t cnt, loff_t *offt) {
    int retvalue = 0;

    memcpy(readbuf, kerneldata, sizeof(kerneldata));
    retvalue = copy_to_user(buf, readbuf, cnt);
    if(retvalue == 0) {
        printk("kernel senddata ok!\r\n");
    }
    else {
        printk("kernel senddata failed!\r\n");
    }

    return 0;
}

static ssize_t chrdevbase_write(struct file *filp, const char __user *buf, size_t cnt, loff_t *offt) {
    int retvalue = 0;
    retvalue = copy_from_user(writebuf, buf, cnt);
    if(retvalue == 0) {
        printk("kernel recevdata:%s\r\n", writebuf);
    }
    else {
        printk("kernel recevdata failed!\r\n");
    }

    return 0;
}

static long chrdevbase_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
    int size = 1300;

    switch (cmd)
    {
    case CHRDEVBASE_GET_SIZE:
        copy_to_user((int*)arg, &size, sizeof(int));
        break;
    
    default:
        return -ENOTTY;
    }
}

static int chrdevbase_release(struct inode *inode, struct file *filp) {
    return 0;
}

// 这个结构体很关键，是设备的操作函数集合
static struct file_operations chrdevbase_fops = {
    .owner = THIS_MODULE,
    .open = chrdevbase_open,
    .read = chrdevbase_read,
    .write = chrdevbase_write,
    .release = chrdevbase_release,
    .unlocked_ioctl = chrdevbase_ioctl,
};

static int __init chrdevbase_init(void) {
    int retvalue = 0;
    dev_t curr_dev;
    // retvalue = register_chrdev(CHRDEVBASE_MAJOR, CHRDEVBASE_NAME, &chrdevbase_fops);
    // if(retvalue < 0) {
    //     printk("chrdevbase driver register failed\r\n");
    // }
    // printk("chrdevbase_init()\r\n");

    /* 初始化cdev */
    cdev_init(&chrdevbase_cdev, &chrdevbase_fops);
    chrdevbase_cdev.owner = THIS_MODULE;

    /* 自动获取字符设备号*/
    alloc_chrdev_region(&dev_num, 0, 1, CHRDEVBASE_NAME);

    /* 注册设备 */
    retvalue = cdev_add(&chrdevbase_cdev, dev_num, 1);
    if(retvalue < 0)
    {
        printk("cdev_add failed\n\r");
    }

    /* 创建设备的类，在/sys/class文件夹下 */
    chrdevbase_class = class_create(THIS_MODULE, CHRDEVBASE_NAME);
    
    /* 自动创建设备节点文件，在/dev文件夹下 */
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num));
    device_create(chrdevbase_class, NULL, curr_dev, NULL, CHRDEVBASE_NAME);

    printk("chrdevbase init\n\r");

    return 0;
}

static void __exit chrdevbase_exit(void) {
    dev_t curr_dev;
    // unregister_chrdev(CHRDEVBASE_MAJOR, CHRDEVBASE_NAME);
    // printk("chrdevbase_exit()\r\n");
    
    /* 删除设备节点文件 */
    curr_dev = MKDEV(MAJOR(dev_num), MINOR(dev_num));
    device_destroy(chrdevbase_class, curr_dev);
    
    /* 删除设备的类 */
    class_destroy(chrdevbase_class);

    /* 注销设备 */
    cdev_del(&chrdevbase_cdev);
    
    /* 释放占用的设备号 */
    unregister_chrdev_region(dev_num, 1);
 
    printk("chrdevbase_exit\n\r");
}

module_init(chrdevbase_init);
module_exit(chrdevbase_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sigmapoet");

