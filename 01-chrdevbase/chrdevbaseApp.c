#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "chr_ioctl.h"

static char usrdata[] = {"sigmapoet data!"};

int main(int argc, char** argv) {
    int fd, retvalue;
    char *filename;
    char readbuf[100], writebuf[100];

    if(argc != 3) {
        printf("Error Usage!\n\r");
        return -1;
    }    

    filename = argv[1];

    // 打开驱动文件
    fd = open(filename, O_RDWR);
    if(fd < 0) {
        printf("Can't open file %s\n\r", filename);
        return -1;
    }

    if(atoi(argv[2]) == 1) {
        retvalue = read(fd, readbuf, 50);
        if(retvalue < 0) {
            printf("read file %s failed\n\r", filename);    
        }
        else {
            printf("read data %s\n\r", readbuf);
        }
    }

    if(atoi(argv[2]) == 2) {
        memcpy(writebuf, usrdata, sizeof(usrdata));
        retvalue = write(fd, writebuf, 50);
        if(retvalue < 0) {
            printf("write file %s failed\n\r", filename);
        }
    }

    if(atoi(argv[2]) == 3) {
        int size = 0;
        retvalue = ioctl(fd, CHRDEVBASE_GET_SIZE, &size);
        if(retvalue < 0) {
            printf("ioctl error\n\r");
        }
        else {
            printf("ioctl CHRDEVBASE_GET_SIZE : %d\n\r", size);
        }
    }

    retvalue = close(fd);
    if(retvalue < 0) {
        printf("Can't close file %s\n\r", filename);
        return -1;
    }

    return 0;
}